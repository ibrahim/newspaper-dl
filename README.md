# newspaper-dl

singapore sph newspaper (straitstimes, businesstimes) download scripts in python

## Requirements

* Python 3 

## Installation

* Clone the repo
```
$ git clone https://gitlab.com/ibrahim/newspaper-dl.git 
```

* Create virtual environment from repo directory
```
$ cd newspaper-dl
$ python3 -m venv .venv
```

* Activate virtual environment and install packages
```
$ . .venv/bin/activate
$ pip3 install -r requirements.txt
```

* Execute program
```
$ ./newspaper.py -h
```



## Configuration file

Configuration is stored in `newspaper-dl.cfg` file. This file is encrypted with git-secret.
`newspaper-dl.cfg-template` is the template configuration file.

## Copyright and License

Copyright (c) 2019 Mohamed Ibrahim. Code released under the [MIT](LICENSE) license.
