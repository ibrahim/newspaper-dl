#!/usr/bin/env python3

import newspaper
import datetime
import hashlib
import requests
import json
import base64
import binascii
from PyPDF3 import PdfFileMerger,PdfFileWriter
import tempfile
import os
import shutil

class BusinessTimes(newspaper.Newspaper):

    def __init__(self, config):
        super(BusinessTimes, self).__init__(config)
        self.newspaper = "businesstimes"        

        localconfig = config[self.newspaper]
        self.url  = localconfig['url']
        self.ua   = localconfig['user_agent']
        self.auth = (localconfig['username'], localconfig['password'])
        self.headers = {  }

    def format_download_date(self,date):
        return date.strftime("%Y-%m-%d")

    def get_download_filename(self,date):
        return "BT{}.pdf".format(date.strftime("%Y%m%d")) 

    def get_server_json(self,date):
        dl_date = self.format_download_date(date)        
        url = self.url.format(dl_date=dl_date)
        r = requests.get(url = url, headers = self.headers, auth = self.auth)
        return json.loads(r.content)

            
    def dump_json(self,date):
       parsed_json = self.get_server_json(date)
       print(json.dumps(parsed_json, indent=4, sort_keys=True))


    def download(self,date,path):
        dl_date = self.format_download_date(date)
        parsed_json = self.get_server_json(date)

        tempdir = tempfile.mkdtemp(suffix='sph')        

        pdfs = []
        for section in parsed_json['SPH-BT']:
            baseurl = section['url']
            for pdf in section['pdfs']:                
                r = requests.get(url = "{}/{}".format(baseurl,pdf), headers = self.headers, auth = self.auth)
                pdf_filename = os.path.join(tempdir,pdf)
                pdfs.append(pdf_filename)
                with open(pdf_filename,"wb") as f:
                    f.write(r.content)
                    f.close()
        pdf_filename = os.path.join(path,self.get_download_filename(date))        
        
        if len(pdfs) > 0:            
            pypdf = PdfFileMerger()            
            for pdf in pdfs:
                pypdf.append(pdf)

            with open(pdf_filename, "wb") as pdf_out_file:
                pypdf.write(pdf_out_file)
                pypdf.close()                
        else:
            # create a blank page pdf if no pdfs are available
            pypdf = PdfFileWriter()
            pypdf.addBlankPage(57.89,81.83)
            with open(pdf_filename, "wb") as pdf_out_file:
                pypdf.write(pdf_out_file)

        shutil.rmtree(tempdir)

