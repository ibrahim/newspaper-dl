#!/usr/bin/env python3

import datetime
import argparse
import os
import configparser

class Newspaper(object):

    def __init__(self, config):

        # strip double quotes ("") from values in config file if found
        # NOTE: do not strip password field, as password fields could start with "
        for section in config.sections():
            for key in config[section]:
                # do not strip password field
                config.set(section,key,config[section][key].strip('"') if key != "password" else config[section][key])

        self.newspaper = ""
        self.url       = ""
        self.ua        = ""
        self.dldate    = datetime.date.today() 
        self.today     = datetime.date.today()

    def parse(self):
        print("parse")

    @staticmethod
    def parse_args():
        choices = {}
        choices['newspaper'] = [ 'st', 'bt' ]

        parser = argparse.ArgumentParser(description='SPH PDF Newspaper downloader')
        parser.add_argument("-n","--newspaper", choices=choices['newspaper'], metavar='', default="st", help="newspaper to download: st|bt")
        parser.add_argument("-j","--jsondump", action='store_true', help="dump json data only")
        parser.add_argument("-p","--path", type=Newspaper.check_path, metavar="path", default=os.getcwd(), help="path to save downloaded pdf")
        parser.add_argument("-d","--date", type=Newspaper.check_arg_date, metavar="date", required=True, help="download date in YYYYMMDD format")
        args = vars( parser.parse_args())
    
        # load config - hash, url, user-agent, auth details, etc
        config = configparser.ConfigParser()
        dataset = config.read(['newspaper-dl.cfg', os.path.expanduser('~/.newspaper-dl.cfg')],encoding='ascii')
        if len(dataset) == 0:
            raise ValueError("Failed to open config file newspaper-dl.cfg")

        paper = Newspaper.factory(args['newspaper'],config)
        
        if args['jsondump'] == True:
            paper.dump_json(args['date'])
        else:
            paper.download(args['date'],args['path'])

    @staticmethod    
    def factory(paper,config):
        # to prevent circular import, move import statements here
        # https://stackoverflow.com/questions/744373/circular-or-cyclic-imports-in-python
        if paper == "st": 
            import straitstimes
            return straitstimes.StraitsTimes(config)
        if paper == "bt": 
            import businesstimes
            return businesstimes.BusinessTimes(config) 
        assert 0, "Invalid newspaper type: {}".format(paper)

    @staticmethod
    def check_arg_date(value):
    	if len(value) != 8:
            raise argparse.ArgumentTypeError("{} is an invalid date".format(value))

    	try:
       	    return datetime.datetime.strptime(value, '%Y%m%d')
    	except:
            raise argparse.ArgumentTypeError("{} is an invalid date".format(value))

    @staticmethod
    def check_path(value):
        path = os.path.realpath(value)
        if not os.path.exists(path):
            raise argparse.ArgumentTypeError("{} path does not exist".format(value))
        elif not os.path.isdir(path):
            raise argparse.ArgumentTypeError("{} is not a directory".format(value))

        return path

    def download(self,date,directory):
        pass

    def dump_json(self,date):
        pass

if __name__ == "__main__":
    Newspaper.parse_args()

