#!/bin/bash

today=$(date +"%Y%m%d")
today_dir=${today:0:4}-${today:4:2}-${today:6:2}
gdrive_basedir=/path/to/gdrive/basedir
pyscript=/path/to/python/script
cleandays="21"
newspapers=("st" "bt")

[ $# -eq 1 ] && [ -n "$1" ] && today="$1"

# look through each newspaper, create a directory, download, remove old papers and sync to gdrive using rclone
for n in "${newspapers[@]}"; do     
    mkdir -p ${gdrive_basedir}/$n/${today_dir}
    
    $pyscript -n $n -p ${gdrive_basedir}/$n/${today_dir} -d $today
    
    find $gdrive_basedir/$n -mtime +$cleandays -exec rm {} \;
    find $gdrive_basedir/$n -type d -exec rmdir {} \;
    
    rclone sync $gdrive_basedir/$n gdrive:/news/$n
done
