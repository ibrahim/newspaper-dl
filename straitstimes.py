#!/usr/bin/env python3

import newspaper
import datetime
import hashlib
import requests
import json
import base64
import binascii
from Crypto.Cipher import AES
from PyPDF3 import PdfFileMerger
import tempfile
import os
import shutil

class StraitsTimes(newspaper.Newspaper):

    def __init__(self, config):
        super(StraitsTimes, self).__init__(config)
        self.newspaper = "straitstimes"

        localconfig = config[self.newspaper]
        self.url  = localconfig['url']
        self.ua   = localconfig['user_agent']
        self.auth = (localconfig['username'], localconfig['password'])
        self.token_postfix = localconfig['token_postfix']
        self.md5_prefix = localconfig['md5_prefix'].encode()
        self.headers = { 'projectName': "straitstimesapp" }

    def get_token_hash(self):
        # NOTE: token is always generated from today's date, not the date of newspaper download
        today = datetime.date.today()
        token_hash = hashlib.md5("{}{}".format(today.strftime("%Y-%m-%d"),self.token_postfix).encode())
        #token_hash = hashlib.md5(b("%s%s".format(today.strftime("%Y-%m-%d"),self.token_postfix)))
        return token_hash.hexdigest()

    def format_download_date(self,date):
        return date.strftime("%Y-%m-%d")

    def get_download_filename(self,date,section="classified"):
        return "ST{}{}.pdf".format(date.strftime("%Y%m%d"),"-classified" if section == "classified" else "") 

    def get_server_json(self,date):
        dl_date = self.format_download_date(date)
        token_hash = self.get_token_hash()
        url = self.url.format(dl_date=dl_date,token_hash=token_hash)
        r = requests.get(url = url, headers = self.headers, auth = self.auth)
        return json.loads(r.content)


    def decrypt_data(self, data):
        # define a list of 3 dictionaries
        # do not define this way, the lists will point to the same object -  
        # https://stackoverflow.com/questions/10712002/create-an-empty-list-in-python-with-certain-size
        # md5hash = [{}] * 3
        md5hash = [{} for _ in range(3)]
        md5hash[0]['input'] = bytearray(bytes(self.md5_prefix))
        md5hash[0]['input'].extend(data[8:16])
        h1 = hashlib.md5(md5hash[0]['input'])
        md5hash[0]['output'] =  bytearray(h1.digest())
        md5hash1i = bytearray(h1.digest())


        # do not assign bytearray ref as modifying md5hash[1]['input'] will also modify md5hash[0]['output']
        # clone the data instead
        #md5hash[1]['input'] = md5hash[0]['output']
        md5hash[1]['input'] = bytearray(md5hash[0]['output'])
        md5hash[1]['input'].extend(md5hash[0]['input'])
        h2 = hashlib.md5(md5hash[1]['input'])
        md5hash[1]['output'] = bytearray(h2.digest())
        

        # do not assign bytearray ref as modifying md5hash[2]['input'] will also modify md5hash[1]['output']
        # clone the data instead
        #md5hash[2]['input'] = md5hash[1]['output']
        md5hash[2]['input'] = bytearray(md5hash[1]['output'])
        md5hash[2]['input'].extend(md5hash[0]['input'])
        h3 = hashlib.md5(md5hash[2]['input'])
        md5hash[2]['output'] = bytearray(h3.digest())
        
        k  = bytearray(md5hash[0]['output'])
        k.extend(md5hash[1]['output'])
        iv = bytearray(md5hash[2]['output'])
        cipher = AES.new(k, AES.MODE_CBC, iv=iv)
        return cipher.decrypt(data[16:])
            
    def dump_json(self,date):
       parsed_json = self.get_server_json(date)
       print(json.dumps(parsed_json, indent=4, sort_keys=True))


    def download(self,date,path):
        token_hash = self.get_token_hash()
        dl_date = self.format_download_date(date)
        parsed_json = self.get_server_json(date)

        tempdir = tempfile.mkdtemp(suffix='sph')        
		
        pdfs = []
	    # sectionRank is an integer, newspaper is sorted in ascending order based on sectionRank
	    # classified section has sectionRank=1 with no 'pdfs' member
        for section in sorted(parsed_json, key = lambda section: int(section['sectionRank'])):
            if ('pdfs' in section):
                for pdf in section['pdfs']:
                    r = requests.get(url = pdf['cover'], headers = self.headers, auth = self.auth)
                    b = base64.b64decode(r.content)                    
                    pdf_filename = os.path.join(tempdir,"{}.pdf".format(pdf['title']))
                    pdf_data = self.decrypt_data(bytearray(b))
                    pdfs.append(pdf_filename)
                    with open("{}".format(pdf_filename),"wb") as f:
                        f.write(pdf_data)
                        f.close()

        merger = PdfFileMerger()
        for pdf in pdfs:
            merger.append(pdf)

        merged_pdf_filename = os.path.join(path,self.get_download_filename(date,"main"))
        merger.write(merged_pdf_filename)
        merger.close()

        pdfs = []
        for section in sorted(parsed_json, key = lambda section: int(section['sectionRank'])):            
            if ('headings' in section):            
                for classified_section in sorted(section['headings'], key = lambda classified_section: int(classified_section['sectionRank'])):
                    for pdf in classified_section['pdfs']:
                        r = requests.get(url = pdf['cover'], headers = self.headers, auth = self.auth)
                        b = base64.b64decode(r.content)
                        pdf_filename = os.path.join(tempdir,"{}.pdf".format(pdf['title']))
                        pdf_data = self.decrypt_data(bytearray(b))
                        pdfs.append(pdf_filename)
                        with open("{}".format(pdf_filename),"wb") as f:
                            f.write(pdf_data)
                            f.close()			


        merger = PdfFileMerger()
        for pdf in pdfs:
            merger.append(pdf)

        merged_pdf_filename = os.path.join(path,self.get_download_filename(date,"classified"))
        merger.write(merged_pdf_filename)
        merger.close()

        shutil.rmtree(tempdir)
